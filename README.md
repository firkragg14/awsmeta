A Go program that can be run on an AWS instance to retrieve the instance meta data

Traverses the metadata api and converts the data to a json array that is returned

can be run using

./awsmeta

can also be run as 

./awsmeta -path "/ami-id" to return a subsection of the metadata service as json