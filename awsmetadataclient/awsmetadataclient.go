package awsmetadataclient

import (
	"bytes"
	"context"
	"encoding/json"
	"strings"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/ec2/imds"
)

//NewClient creates a new client via the AWS sdk for talking to the meta data service
func NewClient() (*imds.Client, error) {
	currentContext := context.TODO()
	cfg, err := config.LoadDefaultConfig(currentContext)
	if err != nil {
		return nil, err
	}

	return imds.NewFromConfig(cfg), nil
}

// GetMetaDataAsMap recursivly travels the meta data urls converting to a map struture
func GetMetaDataAsMap(client *imds.Client, path string) (map[string]interface{}, error) {
	rootData, err := GetMetaData(client, path)

	if err != nil {
		return nil, err
	}

	keys := strings.Split(rootData, "\n")

	jsonMap := make(map[string]interface{})

	for _, key := range keys {
		subpath := path + key
		lastChar := subpath[len(subpath)-1:]
		if lastChar == "/" {
			trimmedKey := key[:len(key)-1]
			result, err := GetMetaDataAsMap(client, subpath)

			//some keys end up having  values and some dont so handle both scenarios
			if err != nil {
				jsonMap[trimmedKey] = subpath
			} else {
				jsonMap[trimmedKey] = result
			}

		} else {
			value := GetMetaDataValue(client, subpath)

			if json.Valid([]byte(value)) {
				jsonMap[key] = json.Unmarshal([]byte(value), &jsonMap)
			} else {
				jsonMap[key] = value
			}

		}
	}

	return jsonMap, nil

}

// GetMetaDataValue handles the values at the end of the tree traversal
func GetMetaDataValue(client *imds.Client, path string) string {
	value, err := GetMetaData(client, path)

	if err != nil {
		return ""
	}

	//handle edge case of encoded array confusing the json decode
	if value == "[]" {
		return ""
	}

	return value
}

// GetMetaData can reads the data from a metadata url path
func GetMetaData(client *imds.Client, path string) (string, error) {
	instanceMetaData, err := client.GetMetadata(context.TODO(), &imds.GetMetadataInput{
		Path: path,
	})

	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(instanceMetaData.Content)
	return buf.String(), nil

}
