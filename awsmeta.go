package main

import (
	"awsmeta/awsmetadataclient"
	"encoding/json"
	"flag"
	"fmt"
	"log"
)

func main() {
	specifiedPath := flag.String("path", "", "A / seperated path")
	flag.Parse()

	client, err := awsmetadataclient.NewClient()

	if err != nil {
		log.Printf("Unable to create client: %s\n", err)
	}
	//The meta data service is recursivly processed to retrieve the dataa
	completejson, err := awsmetadataclient.GetMetaDataAsMap(client, *specifiedPath)

	if err != nil {
		log.Fatal("Invalid path specified")
	}

	//the resulting map from traversing the meta data is marshelled into json for output
	json, err := json.Marshal(completejson)

	fmt.Println(string(json))

}
