module awsmeta

go 1.16

require (
	github.com/aws/aws-sdk-go-v2/config v1.1.1
	github.com/aws/aws-sdk-go-v2/feature/ec2/imds v1.0.2
)
